<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function index(){
        $cast = DB::table('cast')->get();
        return view('index', compact('cast'));
    }
    public function create(){
        return view('create');
    }
    public function store(Request $request)
    {
        // dd($request->all());
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }
    public function show($id){
        $show = DB::table('cast')->where('id',$id)->first();
        return view('show', compact('show'));
    }

    public function edit($id){
        $edit = DB::table('cast')->where('id',$id)->first();
        return view('edit', compact('edit'));
    }

    public function update($id, Request $request){
        $query = DB::table('cast')->where('id',$id)->update([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect ('/cast');
    }

    public function delete($id){
        $query = DB::table('cast')->where('id',$id)->delete();
        return redirect ('/cast');
    }
}
