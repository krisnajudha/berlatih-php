<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function welcome(Request $request){
        $nama = $request["nama"];
        $last = $request["last"];
        echo "<h1>Selamat Datang $nama $last</h1><br>";
        echo "<h4>Terimakasih telah bergabung di Sanbercode. Social Media Kita Bersama<h4>";
    }
}
