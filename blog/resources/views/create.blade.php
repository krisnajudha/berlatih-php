@extends('master')

@push('script')

<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Input Cast</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="form" action="/cast" method="POST">
                  @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputnama">Nama Pemain</label>
                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama Pemain">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputumur">Umur</label>
                    <input type="number" name="umur" class="form-control" id="umur" placeholder="Umur">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputbio">Bio</label>
                    <input type="text" name="bio" class="form-control" id="bio" placeholder="Bio">
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>

@endpush