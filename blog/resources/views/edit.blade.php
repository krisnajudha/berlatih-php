@extends('master')

@push('script')

<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Cast {{$edit->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="form" action="/cast/{{$edit->id}}" method="POST">
                  @csrf
                  @method('put')
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputnama">Nama Pemain</label>
                    <input type="text" name="nama" class="form-control" id="nama" value="{{$edit->nama}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputumur">Umur</label>
                    <input type="number" name="umur" class="form-control" id="umur" value="{{$edit->umur}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputbio">Bio</label>
                    <input type="text" name="bio" class="form-control" id="bio" value="{{$edit->bio}}">
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>

@endpush