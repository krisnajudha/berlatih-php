@extends('master')

@push('script')
<table class="table table-bordered">
<div class="display : flex">
  <thead>                  
    <tr>
      <th style="width: 10px">#</th>
      <th>Nama</th>
      <th>Umur</th>
      <th>Bio</th>
      <th>Details</th>
    </tr>
  </thead>
  <tbody>
  @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td><a href="/cast/{{$value->id}}" class="btn btn-primary">Show</a>
                        <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="/cast/{{$value->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger" value="Delete">
                        </form>
                    </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse   
</tbody>
</div>
</table>


@endpush